//import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

import {Button, Row , Col} from 'react-bootstrap';
export default function Banner(){
	return (
		<Row>	
			<Col className= "p-5">
			<h1>Zuitt Coding bootcamp</h1>
			<p>Opportunities for everyone, everywhere.(this component is from banner.js)</p>
			<Button variant="primary">Enroll now!</Button>
			</Col>

		</Row>
		)
}